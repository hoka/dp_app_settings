from django.apps import AppConfig


class Config(AppConfig):
    name = 'dp_settings'

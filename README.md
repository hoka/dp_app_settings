Information
===========
Provides a table "dp_settings" to store settings in a general way.
You can store strings and integers (most common settings) values by using the model.

String is provided through value_string (TextField).
Integer is provided through value_int (IntegerField).

Installation
============

pip install dp-settings

Example Code
============

```
object_ = DpSettings.objects.create()
object_.app_name = 'dp_settings'
object_.key = 'active'
object_.value_int = 1

object_ = DpSettings.objects.create()
object_.app_name = 'dp_settings'
object_.key = 'email_from'
object_.value_string = 'test@test.de'
```